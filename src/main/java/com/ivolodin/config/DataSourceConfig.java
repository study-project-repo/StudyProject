package com.ivolodin.config;

import lombok.SneakyThrows;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;
import java.net.URI;

@Configuration
@Profile("prod")
public class DataSourceConfig {
    @Bean
    public DataSource getDataSource(){
        var dataSourceBuilder = DataSourceBuilder.create();

        dataSourceBuilder.driverClassName("org.postgresql.Driver");
        dataSourceBuilder.username(System.getenv("DATABASE_USERNAME"));
        dataSourceBuilder.password(System.getenv("DATABASE_PASSWORD"));
        dataSourceBuilder.url(getURL());

        return dataSourceBuilder.build();
    }

    @SneakyThrows
    private String getURL() {
        URI dbUri = new URI(System.getenv("DATABASE_URL"));
        return "jdbc:postgresql://" + dbUri.getHost() + dbUri.getPath();
    }
}
